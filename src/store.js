import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './components/reducers/rootReducer';

const store = createStore(rootReducer,
  composeWithDevTools(),
);

export { store };
