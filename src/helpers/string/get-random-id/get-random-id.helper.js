const getRandomId = () => String(Date.now());

export { getRandomId };
