import React from 'react';
import './App.css';
import logo from './logo.svg';
import Chat from './components/Chat';
import NotFound from './components/not-found/not-found';
import { AppPath } from './common/enums/app/app-path.enum';
import UserList from './components/users/UserList';
import { LoginForm } from './components/validatePage/loginForm';

function App() {
  const { pathname } = window.location;

  const getScreen = (path) => {

    switch (path) {
      case AppPath.ROOT: {
        return <LoginForm />;
      }
      case AppPath.MESSAGES: {
        return <Chat />;
      }
      case AppPath.USER_PAGE: {
        return <UserList />;
      }
      default: {
        return <NotFound />;
      }
    }
  };

  return (
    <>
      <div className='logo'>
        <img src={logo} alt='logo' />
        <h3>Messenger</h3>
      </div>
      <main>{getScreen(pathname)}</main>
    </>
  );
}

export default App;
