const AppPath = {
  ROOT: '/',
  MESSAGES: '/messages',
  USER_PAGE: '/users',
};

export { AppPath };
