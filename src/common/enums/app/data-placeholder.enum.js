const DataPlaceholder = {
  NO_MESSAGES: 'There are no messages',
  NO_MESSAGE: 'Opps... there is no such message',
  PAGE_NOT_FOUND: 'Oops... page not found',
};

export { DataPlaceholder };
