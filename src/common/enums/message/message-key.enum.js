const MessageKey = {
  ID: 'id',
  TITLE: 'title',
  DESCRIPTION: 'description',
  PRIORITY: 'priority',
  STATUS: 'status',
};

export { MessageKey };
