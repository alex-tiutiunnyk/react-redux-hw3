import PropTypes from 'prop-types';
import { MessageKey } from 'common/enums/enums';

const messageType = PropTypes.exact({
  [MessageKey.ID]: PropTypes.string.isRequired,
  [MessageKey.TITLE]: PropTypes.string.isRequired,
  [MessageKey.DESCRIPTION]: PropTypes.string.isRequired,
});

export { messageType };
