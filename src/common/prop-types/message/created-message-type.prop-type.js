import PropTypes from 'prop-types';
import { MessageKey } from 'common/enums/enums';

const createdMessageType = PropTypes.exact({
  [MessageKey.TITLE]: PropTypes.string.isRequired,
  [MessageKey.DESCRIPTION]: PropTypes.string.isRequired,
});

export { createdMessageType };
