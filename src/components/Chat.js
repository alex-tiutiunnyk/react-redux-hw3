import React from 'react';
import Header from './messages/Header';
import MessageInput from './messages/MessageInput';
import MessageList from './messages/MessageList';
import '../App.css';

function Chat() {

  return (
    <div>
      <Header />
      <MessageList />
      <MessageInput />
    </div>
  );
}

export default Chat;
