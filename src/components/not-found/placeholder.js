import PropTypes from 'prop-types';
import './placeholder.css';

export const Placeholder = ({ text }) => <p className='placeholder'>{text}</p>;

Placeholder.propTypes = {
  text: PropTypes.string.isRequired,
};

