import {combineReducers} from 'redux';
import messages from '../messages/reducer';
import messagePage from '../messagePage/reducer';
import users from '../users/reducer.js';
import userPage from '../userPage/reducer.js';

const rootReducer = combineReducers({
    messages,
    messagePage,
    users,
    userPage
});

export default rootReducer;
