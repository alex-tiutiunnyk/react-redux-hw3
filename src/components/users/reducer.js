import { ADD_USER, UPDATE_USER, DELETE_USER } from "./actionTypes";

const initialState = [{
  id: 5,
  name: 'Maya',
  surname: 'Greyson',
  email: 'maya@gmail.com',
  password: '123'
}, {
  id: 8,
  name: 'Tom',
  surname: 'Shooter',
  email: 'tom@gmail.com',
  password: 'admin'
}, {
  id: 10,
  name: 'Wendy',
  surname: 'Lay',
  email: 'wendy@gmail.com',
  password: '123123'
}
];

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_USER: {
      const { id, data } = action.payload;
      const newUser = { id, ...data };
      return [...state, newUser];
    }

    case UPDATE_USER: {
      const { id, data } = action.payload;
      const updatedUsers = state.map(user => {
        if (user.id === id) {
          return {
            ...user,
            ...data
          };
        } else {
          return user;
        }
      });

      return updatedUsers;
    }

    case DELETE_USER: {
      const { id } = action.payload;
      const filteredUsers = state.filter(user => user.id !== id);
      return filteredUsers;
    }

    default:
      return state;
  }
}
