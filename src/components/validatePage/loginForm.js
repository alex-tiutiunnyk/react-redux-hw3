import React from 'react';
import '../styles/validation.css';
import { Link } from '@material-ui/core';
import { AppPath } from '../../common/enums/app/app-path.enum';


export class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
    };
  }

  validate = () => {
    const { login, password } = this.state;
    return login === 'admin' && password === 'admin';
  };


  render() {
    const { login, password } = this.state;
    return (
      <div className='form'>
        <form className='loginFrom' onClick={(e) => this.validate()}>
          <label htmlFor='login'>Login</label>
          <input
            id='inputLogin'
            name='login'
            type='text'
            placeholder='Enter your login'
            value={login}
            onChange={this.handleChange}
          />
          <label htmlFor='password'>Password</label>
          <input
            id='inputPwd'
            name='password'
            type='password'
            placeholder='Enter your password'
            value={password}
            onChange={this.handleChange}
          />

          {(this.validate()) ?
            <Link href={AppPath.USER_PAGE}
                  className='btn btn-primary submit'>Login</Link> :
            <Link href={AppPath.MESSAGES}
                  className='btn btn-primary submit'>Login</Link>}
        </form>

      </div>
    );
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };
}
