import React from 'react';
import '../styles/Message.css';
import moment from 'moment';
import { connect } from 'react-redux';
import { likeMessage } from './actions';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.onLike = this.onLike.bind(this);
  }

  onLike(id) {
    this.props.likeMessage(id);
  }

  render() {
    const {
      id, user, avatar, text, createdAt, editedAt, isLiked,
    } = this.props;

    let classes = [];

    let className = isLiked ? 'message-like liked' : 'message-like';
    if (isLiked) {
      classes.push('liked');
    }

    return (
      <div className='message'>
        <div className='left-block'>
          <p className='message-user-name'>{user}</p>
          <img className='message-user-avatar' src={avatar} />
        </div>
        <div>
          <p className='message-text'>{text}</p>
        </div>
        <div className='right-block'>
          <p className='message-time'>
            {editedAt === ''
              ? moment(createdAt).format('LLL')
              : moment(editedAt).format('LLL')}
          </p>
          <button className={className} onClick={(e) => this.onLike(id)}>
            Like &hearts;
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  likeMessage,
};

export default connect(null, mapDispatchToProps)(Message);