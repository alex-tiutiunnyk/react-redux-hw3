import React from "react";
import "../styles/MessageInput.css";
import PropTypes from "prop-types";
import Modal from "../editPage/EditModal";
import {addMessage} from "./actions";
import {connect} from "react-redux";

class MessageInput extends React.Component {
    constructor(props) {
        super(props);
        this.onAdd = this.onAdd.bind(this);
        this.text = this.props.text;
        this.state = {
            value: ''
        }
    }

    onAdd(event) {
        event.preventDefault();
        let date = new Date();
        this.props.addMessage({userId: 18, createdAt: date, editedAt: '', text: this.state.text});
        this.setState({text: ''});
    }

    render() {
        return (
            <form
                className="message-input"
                onSubmit={this.onAdd}
            >
                <input
                    className="message-input-text"
                    placeholder="Message"
                    value={this.state.text}
                    onChange={(event) => {
                        this.setState({text: event.target.value})
                    }}
                />
                <button className="message-input-button" type="submit" onClick={this.onAdd}>
                    Send
                </button>

            </form>
        );
    }
}

Modal.propTypes = {
    text: PropTypes.string,
    onAdd: PropTypes.func
};
const mapDispatchToProps = {
    addMessage
};
export default connect(null, mapDispatchToProps)(MessageInput);
