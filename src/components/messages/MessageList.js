import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Divider } from 'rsuite';
import moment from 'moment';
import * as actions from './actions';
import Message from './Message';
import OwnMessage from './OwnMessage';
import 'rsuite/dist/styles/rsuite-default.css';
import '../styles/MessageList.css';
import { hidePage, showPage } from '../messagePage/actions';
import InputModal from '../editPage/EditModal';

class MessageList extends Component {
  constructor(props) {
    super(props);
    this.props.messages.sort(function(a, b) {
      return moment(a.createdAt) - moment(b.createdAt);
    });
    this.isLast = this.props.messages.isLast;
  }

  render() {
    let previousDate = null;

    const myMessages = this.props.messages.filter(message =>
      message.userId === 18,
    );
    const lastItem = myMessages[myMessages.length - 1];

    return (
      <div>
        <div className='message-list'>
          {this.props.messages.map(message => {
            let divider = false;
            let currentDate = new Date(Date.parse(message.createdAt));
            if (
              previousDate == null ||
              previousDate.getFullYear() !== currentDate.getFullYear() ||
              previousDate.getMonth() !== currentDate.getMonth() ||
              previousDate.getDay() !== currentDate.getDay()
            ) {
              previousDate = currentDate;
              divider = true;
            }

            if (message.userId !== 18)
              return (
                <div>
                  {divider ? (
                    <Divider>
                      {moment(message.createdAt).format('MMMM Do YYYY')}
                    </Divider>
                  ) : null}
                  <Message
                    key={message.id}
                    id={message.id}
                    user={message.user}
                    text={message.text}
                    isLiked={message.isLiked}
                    avatar={message.avatar}
                    createdAt={message.createdAt}
                    editedAt={message.editedAt}
                  />
                </div>
              );
            else
              return (
                <div>
                  {divider ? (
                    <Divider>
                      {moment(message.createdAt).format('MMMM Do YYYY')}
                    </Divider>
                  ) : null}
                  <OwnMessage
                    isLast={message.id === lastItem.id}
                    key={message.id}
                    id={message.id}
                    text={message.text}
                    createdAt={message.createdAt}
                  />
                </div>
              );
          })
          }
        </div>

        {this.props.isShown ? <InputModal /> : null}
      </div>
    );
  }
}

const
  mapStateToProps = (state) => {
    return {
      messages: state.messages,
      isShown: state.messagePage.isShown,
    };
  };

const
  mapDispatchToProps = {
    ...actions,
    showPage,
    hidePage,
  };

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);