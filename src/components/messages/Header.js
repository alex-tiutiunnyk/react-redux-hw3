import React from 'react';
import '../styles/Header.css';
import 'moment/min/moment-with-locales.min.js';
import moment from 'moment';
import { connect } from 'react-redux';

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const userCount = [];
    const messageTime = [];

    this.props.messages.map((message) => {
      if (userCount.indexOf(message.userId) === -1) {
        userCount.push(message.userId);
      }
      if (message.editedAt === '') {
        messageTime.push(message.createdAt);
      } else {
        messageTime.push(message.editedAt);
      }
    });

    let length = messageTime.length;

    return (
      <div className='header'>
        <div>
          <p className='header-title'>My chat</p>
          <p className='header-users-count'>
            {userCount.length}&nbsp;participants
          </p>
          <p className='header-messages-count'>
            {this.props.messages.length}&nbsp;messages
          </p>
        </div>
        <div>
          <p className='header-last-message-date'>
            last message at&nbsp;{moment(messageTime[length - 1]).format('LLL')}
          </p>
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
  };
};

export default connect(mapStateToProps, null)(Header);
