import React from 'react';
import '../styles/OwnMessage.css';
import 'moment/min/moment-with-locales.min.js';
import moment from 'moment';
import { connect } from 'react-redux';
import { setCurrentMessageId, showPage } from '../messagePage/actions';
import { deleteMessage, updateMessage } from './actions';

class OwnMessage extends React.Component {
  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  onEdit(id) {
    this.props.setCurrentMessageId(id);
    this.props.showPage();
  }

  onDelete(id) {
    this.props.deleteMessage(id);
  }

  render() {
    const { id, text, createdAt, isLast } = this.props;

    return (
      <div className='own-message'>
        <div>
          <p className='message-text'>{text}</p>
        </div>
        <div className='right-block'>
          <p className='message-time'>{moment(createdAt).format('LT')}</p>

          {isLast ? <button
              className='message-edit'
              onClick={(e) => this.onEdit(id)}>
              Edit &#9998;
            </button>
            : null}

          <button
            className='message-delete'
            onClick={(e) => this.onDelete(id)}
          >
            Delete &#9746;
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  updateMessage,
  deleteMessage,
  setCurrentMessageId,
  showPage,
};
export default connect(null, mapDispatchToProps)(OwnMessage);
