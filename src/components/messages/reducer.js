import { ADD_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE, UPDATE_MESSAGE } from './actionTypes';

const initialState = [{
  id: 1,
  userId: 5,
  avatar: 'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
  user: 'Maya',
  text: 'Maybe...',
  createdAt: '2020-07-17T19:30:14.480Z',
  editedAt: '',
  isLiked: false,
}, {
  id: 2,
  userId: 10,
  avatar: 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ',
  user: 'Wendy',
  text: 'Hello you',
  createdAt: '2020-07-16T19:47:14.480Z',
  editedAt: '2020-07-16T19:48:12.936Z',
  isLiked: true,
}, {
  id: 4,
  userId: 10,
  avatar: 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ',
  user: 'Wendy',
  text: 'Hello you',
  createdAt: '2020-07-14T19:47:14.480Z',
  editedAt: '',
  isLiked: true,
}, {
  id: 3,
  userId: 8,
  avatar: 'https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg',
  user: 'Tom',
  text: 'Bye you',
  createdAt: '2020-07-16T19:48:14.480Z',
  editedAt: '2020-07-16T19:49:14.480Z',
  isLiked: false,
},
];

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_MESSAGE: {
      const { id, data } = action.payload;
      const newMessage = { id, ...data };
      return [...state, newMessage];
    }

    case UPDATE_MESSAGE: {
      const { id, data } = action.payload;
      const updatedMessages = state.map(message => {
        if (message.id === id) {
          return {
            ...message,
            ...data,
          };
        } else {
          return message;
        }
      });

      return updatedMessages;
    }

    case DELETE_MESSAGE: {
      const { id } = action.payload;
      const filteredMessages = state.filter(message => message.id !== id);
      return filteredMessages;
    }

    case LIKE_MESSAGE: {
      const { id } = action.payload;
      const likedMessages = state.map(message => {
        if (message.id === id) {
          return {
            ...message,
            isLiked: !message.isLiked,
          };
        } else {
          return message;
        }
      });

      return likedMessages;
    }
    default:
      return state;
  }
}
