import React from 'react';
import * as actions from '../messagePage/actions';
import { dropCurrentMessageId } from '../messagePage/actions';
import { updateMessage } from '../messages/actions';
import { connect } from 'react-redux';
import './Modal.css';

class EditModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = { message: null };
    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChangeData = this.onChangeData.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.messageId !== prevState?.messageId) {
      const message = nextProps.messages.find(message => message.id === nextProps.messageId);
      return {
        ...nextProps,
        message: message,
      };
    }
    return null;
  }

  onCancel() {
    this.props.dropCurrentMessageId();
    this.props.hidePage();
  }

  onSave() {
    this.props.updateMessage(this.props.messageId, this.state.message);
    this.props.dropCurrentMessageId();
    this.props.hidePage();
  }

  onChangeData(e) {
    const value = e.target.value;
    this.setState(
      {
        message: {
          ...this.state.message,
          text: value,
          editedAt: Date.now(),
        },
      },
    );
  }

  render() {
    return (
      <div className='edit-message-modal'>

        <div className='modal-content'>
          Edit message
          <input className='edit-message-input'
                 value={this.state.message?.text}
                 onChange={this.onChangeData} />

          <div className='buttons'>
            <button className='edit-message-button'
                    onClick={this.onSave}>Submit
            </button>
            <button className='edit-message-close'
                    onClick={this.onCancel}>Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages,
    isShown: state.messagePage.isShown,
    messageId: state.messagePage.messageId,
  };
};

const mapDispatchToProps = {
  ...actions,
  dropCurrentMessageId,
  updateMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditModal);