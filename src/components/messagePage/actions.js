import { DROP_CURRENT_MESSAGE_ID, HIDE_PAGE, SET_CURRENT_MESSAGE_ID, SHOW_PAGE } from './actionTypes';

export const setCurrentMessageId = id => ({
  type: SET_CURRENT_MESSAGE_ID,
  payload: {
    id,
  },
});

export const dropCurrentMessageId = () => ({
  type: DROP_CURRENT_MESSAGE_ID,
});

export const showPage = () => ({
  type: SHOW_PAGE,
});

export const hidePage = () => ({
  type: HIDE_PAGE,
});